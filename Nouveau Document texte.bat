:start
:: la pop up pour savoir si on veux regarder la tv
@echo off
set "%%f"="7"
for /f "usebackq" %%f in (
		`mshta "javascript:new ActiveXObject('Scripting.FileSystemObject').GetStandardStream(1).Write(new ActiveXObject('WScript.Shell').PopUp('Voulez vous regarder la tv',0,'TV',36));close();"`
	) do (
		if "%%f"=="6" (
			GOTO oui
		) else if "%%f"=="7" (
			GOTO non
		) else (
			goto start
		)
	)


:: Si la réponse est non
:non
echo non
:: remet le temps de veille de l'écran à 5min et 10min pour le pc
powercfg -CHANGE -monitor-timeout-ac 5
powercfg -CHANGE -standby-timeout-ac 10
powercfg -CHANGE -monitor-timeout-dc 5
powercfg -CHANGE -standby-timeout-dc 10
goto fin



:: Si la réponse est oui
:oui
echo oui
cd C:\ProgramData\Microsoft\Windows\Start Menu\Programs\BlueStacks X\
"BlueStacks X.lnk"
:: enleve le temps de veille de l'écran et de l'ordi
powercfg -CHANGE -monitor-timeout-ac 0
powercfg -CHANGE -standby-timeout-ac 0
powercfg -CHANGE -monitor-timeout-dc 0
powercfg -CHANGE -standby-timeout-dc 0
GOTO start

:fin
timeout 3600



